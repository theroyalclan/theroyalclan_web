package com.royalclan.common.cache;

/**
 * @author 
 *
 */
public interface CacheFactory {

	Cache getCache(String cacheName);

	Cache getCache(String cacheName, int timeToLiveSeconds, int maxElementsInCache);	
}
