package com.royalclan.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.handler.EventHandler;
import com.royalclan.model.Event;
import com.royalclan.pagination.Paginator;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.annotations.UnSecure;
import com.royalclan.service.common.WebserviceRequest;
import com.royalclan.service.descriptors.EventPaginationInput;

@Path("/v1/event/")
public class EventService extends BaseService {
	
	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get-events")
	public String getEvents(@Context HttpHeaders headers, @Context UriInfo uriInfo, WebserviceRequest request){
		
		EventPaginationInput eventPaginationInput = (EventPaginationInput) JsonUtil.getObject(request.getPayload(), EventPaginationInput.class);		
		Paginator<Event> events = EventHandler.getInstance().getEvents(eventPaginationInput);
		return JsonUtil.getJsonForListBasedOnDescriptor(events, Event.class, Event.class);
		
	}
	
	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{eventId}")
	public String getEvent(@PathParam(value = "eventId") long eventId, @Context HttpHeaders headers, @Context UriInfo uriInfo) throws ObjectNotFoundException{
	Event event = EventHandler.getInstance().getEvent(eventId);
		return JsonUtil.getJsonBasedOnDescriptor(event, Event.class);
		
	}
	
	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	public String addEvent(@Context HttpHeaders headers, @Context UriInfo uriInfo,WebserviceRequest request) throws ObjectNotFoundException{

		Event event = (Event) JsonUtil.getObject(request.getPayload(), Event.class);
		Event eventSaved = EventHandler.getInstance().addEvent(event);
		return JsonUtil.getJsonBasedOnDescriptor(eventSaved, Event.class);
		
	}
	
	@PUT
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{eventId}")
	public String updateEvent(@PathParam(value = "eventId") long eventId,@Context HttpHeaders headers, @Context UriInfo uriInfo,WebserviceRequest request) throws ObjectNotFoundException{

		Event event = (Event) JsonUtil.getObject(request.getPayload(), Event.class);
		Event eventSaved = EventHandler.getInstance().updateEvent(event);
		return JsonUtil.getJsonBasedOnDescriptor(eventSaved, Event.class);
		
	}
	
	@DELETE
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{eventId}")
	public String deleteEvent(@PathParam(value="eventId") long eventId, @Context HttpHeaders headers, @Context UriInfo uriInfo)
			throws ObjectNotFoundException, BusinessException, EncryptionException {

		boolean deleted = EventHandler.getInstance().deleteEvent(eventId);
		return JsonUtil.getJsonBasedOnDescriptor(deleted,Boolean.class);
	}

}
