package com.royalclan.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.handler.QuoteHandler;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.Quote;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.annotations.UnSecure;

@Path("/v1/quote/")
public class QuoteService extends BaseService {
	
	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get-quotes")
	public String getQuotes(@QueryParam(value="pageNo") int pageNo,@QueryParam(value="pageSize") int pageSize, @Context HttpHeaders headers, @Context UriInfo uriInfo){
		PaginationInput paginationInput = new PaginationInput(pageNo, pageSize);
		 Paginator<Quote> quotes = QuoteHandler.getInstance().getQuotes(paginationInput);

		return JsonUtil.getJsonBasedOnDescriptor(quotes,Quote.class);
		
	}

}
