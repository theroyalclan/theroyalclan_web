package com.royalclan.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.handler.RelationHandler;
import com.royalclan.model.Relation;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.common.WebserviceRequest;

@Path("/v1/relation/")
public class RelationService extends BaseService {

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allRelations")
	public String getRelations(@Context HttpHeaders headers, @Context UriInfo uriInfo)
			throws ObjectNotFoundException, BusinessException, EncryptionException {
		List<Relation> relations = RelationHandler.getInstance().getAllRelations();

		return JsonUtil.getJsonForListBasedOnDescriptor(relations, Relation.class, Relation.class);
	}
}