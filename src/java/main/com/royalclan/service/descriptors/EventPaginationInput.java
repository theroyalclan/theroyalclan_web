package com.royalclan.service.descriptors;

public class EventPaginationInput {
	
	private int pageNo;
	private int pageSize;
	private long userId;
	private boolean upcoming;
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isUpcoming() {
		return upcoming;
	}
	public void setUpcoming(boolean upcoming) {
		this.upcoming = upcoming;
	}
	
	

}
