package com.royalclan.service.descriptors;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.User;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface UserPaginatorDescriptor extends JSONSerializationDescriptor {
	
	    @JsonProperty
	    public long getCount();
	    
	    @JsonProperty("users")
	    @SerializationDescriptor(value = UserListDescriptor.class)
	    public List<User> getList();

	    @JsonProperty
		public int getPageNo();
	    
	    @JsonProperty
		public int getEnd();
	    
	    @JsonProperty
		public int getStart();
}
