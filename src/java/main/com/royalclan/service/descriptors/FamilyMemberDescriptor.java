package com.royalclan.service.descriptors;

import java.sql.Date;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.File;
import com.royalclan.model.Relation;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface FamilyMemberDescriptor {
	
	    @JsonProperty
	    public long getId();
	    
	    @JsonProperty
	    @SerializationDescriptor(value = FileDescriptor.class)
	    public File getFile();
	    
	    @JsonProperty
	    @SerializationDescriptor(value = RelationDescriptor.class)
	    public Relation getRelation();
	    
		@JsonProperty
		public String getName();
		@JsonProperty
		public String getNickName();
		@JsonProperty
		public Date getDateOfBirth();
		@JsonProperty
		public long getRelationId();
		@JsonProperty
		public long getImageId();

}
