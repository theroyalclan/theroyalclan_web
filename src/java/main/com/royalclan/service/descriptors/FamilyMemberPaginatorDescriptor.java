package com.royalclan.service.descriptors;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.FamilyMember;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface FamilyMemberPaginatorDescriptor extends PaginationOutputDescriptor {

	@JsonProperty("familyMembers")
	@SerializationDescriptor(value = FamilyMemberDescriptor.class)
	public List<FamilyMember> getList();
	
	@JsonProperty
	public long getCount();
	
	@JsonProperty
	public int getPageNo();
	
	@JsonProperty
	public int getStart();
	
	@JsonProperty
	public int getEnd();
}
