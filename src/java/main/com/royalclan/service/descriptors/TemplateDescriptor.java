package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.EventType;
import com.royalclan.model.File;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface TemplateDescriptor {
	
	    @JsonProperty
	    public long getId();
	    @JsonProperty
	    @SerializationDescriptor(value = FileDescriptor.class)
	    public File getFile();
	    
	    @JsonProperty
	    @SerializationDescriptor(value = RelationDescriptor.class)
	    public EventType getEventType();
	    

}
