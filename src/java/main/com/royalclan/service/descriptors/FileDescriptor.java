package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

public interface FileDescriptor extends JSONSerializationDescriptor {
	
	    @JsonProperty
	    public long getId();
	    @JsonProperty
		public String getFileName();
	    @JsonProperty
		public String getPath();
	    @JsonProperty
		public String getBasePath();
	    @JsonProperty
		public String getDomainPath();
	    @JsonProperty
		public String getHostPath();

}
