package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.File;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface RelationDescriptor {
	
	    @JsonProperty
	    public long getId();
	    @JsonProperty
	    public String getType();

}
