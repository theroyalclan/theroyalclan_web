package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

public interface PaginationOutputDescriptor extends JSONSerializationDescriptor {

	
	@JsonProperty
	public int getPageNo();



	@JsonProperty
	public long getCount();
}
