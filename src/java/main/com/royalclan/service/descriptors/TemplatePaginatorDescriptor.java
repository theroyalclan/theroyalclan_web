package com.royalclan.service.descriptors;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.Template;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface TemplatePaginatorDescriptor extends JSONSerializationDescriptor {
	
	    @JsonProperty
	    public long getCount();
	    
	    @JsonProperty("templates")
	    @SerializationDescriptor(value = TemplateDescriptor.class)
	    public List<Template> getList();

	    @JsonProperty
		public int getPageNo();
	    
	    @JsonProperty
		public int getEnd();
	    
	    @JsonProperty
		public int getStart();
}
