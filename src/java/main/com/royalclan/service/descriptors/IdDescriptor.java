package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

public interface IdDescriptor {
	
	    @JsonProperty
	    public long getId();

}
