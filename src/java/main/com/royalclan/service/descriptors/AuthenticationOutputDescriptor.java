package com.royalclan.service.descriptors;

public class AuthenticationOutputDescriptor {
	
	private long userId;
	
	private String sessionToken;
	
	

	public AuthenticationOutputDescriptor(long userId, String sessionToken) {
		super();
		this.userId = userId;
		this.sessionToken = sessionToken;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
	
	

}
