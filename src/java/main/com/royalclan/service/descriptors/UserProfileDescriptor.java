package com.royalclan.service.descriptors;

import java.sql.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.FamilyCode;
import com.royalclan.model.File;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface UserProfileDescriptor extends JSONSerializationDescriptor {

	@JsonProperty
	public long getId();

	@JsonProperty
	public String getFirstName();
	@JsonProperty
	public String getEmail();
	@JsonProperty
	@JsonIgnore
	public String getPassword();
	@JsonProperty
	public String getLastName();
	@JsonProperty
	public String getNickName();
	@JsonProperty
	public String getPhoneNumber();
	@JsonProperty
	public Boolean getGender();
	@JsonProperty
	public Date getDateOfBirth();
	@JsonProperty
	public Boolean getMarried();
	@JsonProperty
	public Date getMarriedDate();
	
	@JsonProperty
	@SerializationDescriptor(value = FileDescriptor.class)
	public File getFile();
	
	@JsonProperty
	@SerializationDescriptor(value = FamilyCodeDescriptor.class)
	public FamilyCode getFamilyCode();

}
