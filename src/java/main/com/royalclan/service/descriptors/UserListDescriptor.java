package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.File;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface UserListDescriptor extends JSONSerializationDescriptor {

	
	@JsonProperty
	public long getId();

	@JsonProperty
	public String getFirstName();
	@JsonProperty
	public String getEmail();

	@JsonProperty
	public String getLastName();
	@JsonProperty
	public String getNickName();
	@JsonProperty
	public String getPhoneNumber();
	@JsonProperty
	public String getImageId();

	
	@JsonProperty
	@SerializationDescriptor(value = FileDescriptor.class)
	public File getFile();
}
