package com.royalclan.service.descriptors;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.File;
import com.royalclan.service.annotations.SerializationDescriptor;

public interface FamilyCodeDescriptor {
	
	    @JsonProperty
	    public long getId();
	    @JsonProperty
	    public String getCode();
	    @JsonProperty
	    @SerializationDescriptor(value = FileDescriptor.class)
	    public File getFile();
	    
	    @JsonProperty
	    public long getAvatarFileId();

}
