package com.royalclan.service;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.handler.TemplateHandler;
import com.royalclan.model.Template;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.PaginationOutput;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.descriptors.TemplatePaginatorDescriptor;

@Path("/v1/template/")
public class TemplateService extends BaseService {

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get-templates")
	public String getTemplates(@DefaultValue("1") @QueryParam(value="pageNo") int pageNo,@DefaultValue("10") @QueryParam(value="pageSize") int pageSize, @Context HttpHeaders headers, @Context UriInfo uriInfo){
		PaginationInput paginationInput = new PaginationInput(pageNo, pageSize);
		 PaginationOutput<Template> templates = TemplateHandler.getInstance().getTemplates(paginationInput);
//		 PaginationOutput<Template> output = new PaginationOutput<>(templates, pageNo, pageSize);

		return JsonUtil.getJsonBasedOnDescriptor(templates,TemplatePaginatorDescriptor.class);
		
	}
}
