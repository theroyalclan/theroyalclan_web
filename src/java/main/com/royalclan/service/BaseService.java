package com.royalclan.service;

import javax.ws.rs.CookieParam;

/**
 * Base Service for all the RESTFUL Services
 * 
 * @author
 *
 */
public class BaseService {

	public static final String SESSION_TOKEN_NAME = "royalClanSessionId";

	@CookieParam(SESSION_TOKEN_NAME)
	String royalClanSessionId;

	/*
	 * Get the session id for the user session
	 */

	public BaseService() {
	}

	public String getSessionId() {
		return royalClanSessionId;
	}

}
