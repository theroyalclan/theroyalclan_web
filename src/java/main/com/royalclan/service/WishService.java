package com.royalclan.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.handler.WishHandler;
import com.royalclan.model.Wish;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.annotations.UnSecure;
import com.royalclan.service.common.WebserviceRequest;

@Path("/v1/wish/")
public class WishService extends BaseService {
	
	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/get-wishes")
	public String getWishs(@QueryParam(value="pageNo") int pageNo,@QueryParam(value="pageSize") int pageSize, @Context HttpHeaders headers, @Context UriInfo uriInfo){
		PaginationInput paginationInput = new PaginationInput(pageNo, pageSize);
		 Paginator<Wish> wishes = WishHandler.getInstance().getWishes(paginationInput);

		return JsonUtil.getJsonBasedOnDescriptor(wishes,Wish.class);
		
	}
	
	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	public String addWish(@Context HttpHeaders headers, @Context UriInfo uriInfo,WebserviceRequest request) throws ObjectNotFoundException{

		Wish wishInput = (Wish) JsonUtil.getObject(request.getPayload(), Wish.class);
		Wish wishSaved = WishHandler.getInstance().addWish(wishInput);
		return JsonUtil.getJsonBasedOnDescriptor(wishSaved, Wish.class);
		
	}
	
	
	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{wishId}")
	public String getWish(@PathParam(value = "wishId") long wishId, @Context HttpHeaders headers, @Context UriInfo uriInfo) throws ObjectNotFoundException{
	Wish wish = WishHandler.getInstance().getWish(wishId);
		return JsonUtil.getJsonBasedOnDescriptor(wish, Wish.class);
		
	}

}
