package com.royalclan.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.royalclan.common.json.JsonUtil;
import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.handler.UserHandler;
import com.royalclan.handler.user.AuthenticationHandlerFactory;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.User;
import com.royalclan.model.UserSessionToken;
import com.royalclan.model.user.AuthenticationInput;
import com.royalclan.model.user.AuthenticationOutput;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.PaginationOutput;
import com.royalclan.service.annotations.RestService;
import com.royalclan.service.annotations.ServiceStatus;
import com.royalclan.service.annotations.UnSecure;
import com.royalclan.service.common.ServiceRequestContextHolder;
import com.royalclan.service.common.WebserviceRequest;
import com.royalclan.service.descriptors.AuthenticationOutputDescriptor;
import com.royalclan.service.descriptors.ChangePassword;
import com.royalclan.service.descriptors.FamilyMemberDescriptor;
import com.royalclan.service.descriptors.FamilyMemberPaginatorDescriptor;
import com.royalclan.service.descriptors.UserPaginatorDescriptor;
import com.royalclan.service.descriptors.UserProfileDescriptor;

/**
 * @author
 *
 */

@Path("/v1/user/")
public class UserService extends BaseService {

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/checkUser")
	public String checkUser(@Context HttpHeaders headers, @Context UriInfo uriInfo) {
		UserSessionToken sessionToken = ServiceRequestContextHolder.getContext().getUserSessionToken();

		AuthenticationOutputDescriptor authenticationOutputDescriptor = new AuthenticationOutputDescriptor(
				sessionToken.getUserId(), sessionToken.getUserSessionId());

		return JsonUtil.getJsonBasedOnDescriptor(authenticationOutputDescriptor, AuthenticationOutputDescriptor.class);
	}

	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@UnSecure
	public String register(@Context HttpHeaders headers, @Context UriInfo uriInfo, WebserviceRequest request)
			throws ObjectNotFoundException, BusinessException, EncryptionException {

		User user = (User) JsonUtil.getObject(request.getPayload(), User.class);
		AuthenticationOutput authOutput = UserHandler.getInstance().register(user);

		AuthenticationOutputDescriptor authenticationOutputDescriptor = new AuthenticationOutputDescriptor(
				authOutput.getUser().getId(), authOutput.getSessionToken());

		return JsonUtil.getJsonBasedOnDescriptor(authenticationOutputDescriptor, AuthenticationOutputDescriptor.class);
	}

	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/signIn")
	@UnSecure
	public String signIn(@Context HttpHeaders headers, @Context UriInfo uriInfo, WebserviceRequest request)
			throws ObjectNotFoundException, BusinessException, EncryptionException {

		AuthenticationInput input = (AuthenticationInput) JsonUtil.getObject(request.getPayload(),
				AuthenticationInput.class);

		AuthenticationOutput output = AuthenticationHandlerFactory.getAuthenticationHandler(input.getAuthType())
				.authenticate(input);
		AuthenticationOutputDescriptor authenticationOutputDescriptor = new AuthenticationOutputDescriptor(
				output.getUser().getId(), output.getSessionToken());

		return JsonUtil.getJsonBasedOnDescriptor(authenticationOutputDescriptor, AuthenticationOutputDescriptor.class);
	}

	/*
	 * @POST
	 * 
	 * @RestService(input = String.class, output = String.class)
	 * 
	 * @ServiceStatus(value = "complete")
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON)
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Path("/test")
	 * 
	 * @UnSecure public String test(@Context HttpHeaders headers, @Context
	 * UriInfo uriInfo, WebserviceRequest request) throws
	 * ObjectNotFoundException, BusinessException, EncryptionException {
	 * 
	 * User user = (User) JsonUtil.getObject(request.getPayload(), User.class);
	 * 
	 * String outputString =
	 * "{\"status\": \"SUCCESS\", \"payload\": \"Test Successful\"}"; return
	 * outputString; }
	 */

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}")
	public String getLoggedInUserDetails(@PathParam(value = "userId") long userIdPath, @Context HttpHeaders headers,
			@Context UriInfo uriInfo) throws ObjectNotFoundException, BusinessException, EncryptionException {
		long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
		User user = (User) UserHandler.getInstance().getLoggedInUser(userId);
		return JsonUtil.getJsonBasedOnDescriptor(user, UserProfileDescriptor.class);
	}

	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/forgotPassword")
	@UnSecure
	public String forgotPassword(@Context HttpHeaders headers, @Context UriInfo uriInfo, WebserviceRequest request)
			throws ObjectNotFoundException, BusinessException, EncryptionException {
		User user = (User) JsonUtil.getObject(request.getPayload(), User.class);
		UserHandler.getInstance().forgotPassword(user.getEmail());
		return JsonUtil.getJsonBasedOnDescriptor(true, Boolean.class);
	}

	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/changePassword")
	public String changePassword(@Context HttpHeaders headers, @Context UriInfo uriInfo, WebserviceRequest request)
			throws EncryptionException, BusinessException {
		ChangePassword changePassword = (ChangePassword) JsonUtil.getObject(request.getPayload(), ChangePassword.class);
		UserHandler.getInstance().changePassword(changePassword);
		return JsonUtil.getJsonBasedOnDescriptor(true, Boolean.class);
	}

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/logout")
	public String logout(@Context HttpHeaders headers, @Context UriInfo uriInfo) {
		boolean isLogout = UserHandler.getInstance().logout();
		return JsonUtil.getJsonBasedOnDescriptor(isLogout, Boolean.class);
	}

	@PUT
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}")
	public String update(@PathParam(value = "userId") long userIdPath, @Context HttpHeaders headers,
			@Context UriInfo uriInfo, WebserviceRequest request)
			throws ObjectNotFoundException, BusinessException, EncryptionException {

		User user = (User) JsonUtil.getObject(request.getPayload(), User.class);
		user.setId(ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId());

		User output = (User) UserHandler.getInstance().updateUser(user);

		return JsonUtil.getJsonBasedOnDescriptor(output, UserProfileDescriptor.class);
	}

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}/familyMembers")
	public String getFamilyMembers(@PathParam(value = "userId") long userIdPath,
			@DefaultValue("1") @QueryParam(value = "pageNo") int pageNo,@DefaultValue("10") @QueryParam(value = "pageSize") int pageSize,
			@Context HttpHeaders headers, @Context UriInfo uriInfo)
			throws ObjectNotFoundException, BusinessException, EncryptionException {
		PaginationInput paginationInput = new PaginationInput(pageNo, pageSize);
		long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
		PaginationOutput<FamilyMember> familyMembers = UserHandler.getInstance().getFamilyMembers(paginationInput, userId);
//		PaginationOutput<FamilyMember> output = new PaginationOutput<>(familyMembers, pageNo, pageSize);

		return JsonUtil.getJsonBasedOnDescriptor(familyMembers, FamilyMemberPaginatorDescriptor.class);
	}

	@POST
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}/familyMember")
	public String addFamilyMembers(@PathParam(value = "userId") long userIdPath, @Context HttpHeaders headers,
			@Context UriInfo uriInfo, WebserviceRequest request)
			throws ObjectNotFoundException, BusinessException, EncryptionException {
		FamilyMember familyMember = (FamilyMember) JsonUtil.getObject(request.getPayload(), FamilyMember.class);
		familyMember.setUserId(ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId());
		FamilyMember familyMemberAdded = UserHandler.getInstance().addFamilyMember(familyMember);
		return JsonUtil.getJsonBasedOnDescriptor(familyMemberAdded, FamilyMemberDescriptor.class);
	}

	@PUT
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}/familyMember/{memberId}")
	public String updateFamilyMembers(@PathParam(value = "userId") long userIdPath,
			@PathParam(value = "memberId") long memberId, @Context HttpHeaders headers, @Context UriInfo uriInfo,
			WebserviceRequest request) throws ObjectNotFoundException, BusinessException, EncryptionException {
		FamilyMember familyMember = (FamilyMember) JsonUtil.getObject(request.getPayload(), FamilyMember.class);
		familyMember.setUserId(ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId());
		FamilyMember familyMemberAdded = UserHandler.getInstance().updateFamilyMember(familyMember);
		return JsonUtil.getJsonBasedOnDescriptor(familyMemberAdded, FamilyMemberDescriptor.class);
	}

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}/familyMember/{memberId}")
	public String getFamilyMember(@PathParam(value = "userId") long userIdPath,
			@PathParam(value = "memberId") long memberId, @Context HttpHeaders headers, @Context UriInfo uriInfo
				) throws ObjectNotFoundException, BusinessException, EncryptionException {

		FamilyMember familyMember = UserHandler.getInstance().getFamilyMember(memberId);
		return JsonUtil.getJsonBasedOnDescriptor(familyMember, FamilyMemberDescriptor.class);
	}

	@DELETE
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userId}/familyMember/{memberId}")
	public String deleteFamilyMembers(@PathParam(value = "userId") long userIdPath,
			@PathParam(value = "memberId") long memberId, @Context HttpHeaders headers, @Context UriInfo uriInfo
			) throws ObjectNotFoundException, BusinessException, EncryptionException {

		boolean familyMember = UserHandler.getInstance().deletFamilyMember(memberId);
		return JsonUtil.getJsonBasedOnDescriptor(familyMember, Boolean.class);
	}

	@GET
	@RestService(input = String.class, output = String.class)
	@ServiceStatus(value = "complete")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/allUsers")
	public String getAllUsers(@DefaultValue("1") @QueryParam(value = "pageNo") int pageNo,
			@DefaultValue("10") @QueryParam(value = "pageSize") int pageSize, @Context HttpHeaders headers, @Context UriInfo uriInfo
			) throws ObjectNotFoundException, BusinessException, EncryptionException {
		PaginationInput paginationInput = new PaginationInput(pageNo, pageSize);
		long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
		PaginationOutput<User> users = UserHandler.getInstance().getAllUsers(paginationInput, userId);
		

		return JsonUtil.getJsonBasedOnDescriptor(users, UserPaginatorDescriptor.class);
	}

}