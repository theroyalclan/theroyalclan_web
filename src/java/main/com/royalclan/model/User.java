package com.royalclan.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

/**
 * @author
 *
 */
@Entity
@Table(name = "USER")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class User extends AbstractObject {
	
	public final static String LABEL_EMAIL="email";
	public final static String LABEL_PASSWORD="password";
	public final static String LABEL_FIRST_NAME="firstName";
	public final static String LABEL_LAST_NAME="lastName";
	public final static String LABEL_NICK_NAME="nickName";
	public final static String LABEL_PHONE_NUMBER="phoneNumber";
	public final static String LABEL_GENDER="gender";
	public final static String LABEL_IMAGE_ID="imageId";
	public final static String LABEL_DATE_OF_BIRTH="dateOfBirth";
	public final static String LABEL_MARRIED="married";
	public final static String LABEL_MARRIED_DATE="marriedDate";
	public final static String LABEL_FAMILY_CODE_ID="familyCodeId";

	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "LAST_NAME")
	private String lastName;
	@Column(name = "NICK_NAME")
	private String nickName;

	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;

	@Column(name = "GENDER")
	private Boolean gender;

	@Column(name = "IMAGE_ID")
	private long imageId;

	@Column(name = "DOB")
	private Date dateOfBirth;

	@Column(name = "MARRIED")
	private Boolean married;

	@Column(name = "MARRIED_DATE")
	private Date marriedDate;

	@Column(name = "FAMILY_CODE_ID")
	private long familyCodeId;
	
	@Transient
	private File file;
	@Transient
	private FamilyCode familyCode;

	public User() {

	}
	
	

	public User(String email, String password, String firstName, String lastName, String nickName, String phoneNumber,
			Boolean gender, long imageId, Date dateOfBirth, Boolean married, Date marriedDate,
			long familyCodeId) {
		super();
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.nickName = nickName;
		this.phoneNumber = phoneNumber;
		this.gender = gender;
		this.imageId = imageId;
		this.dateOfBirth = dateOfBirth;
		this.married = married;
		this.marriedDate = marriedDate;
		this.familyCodeId = familyCodeId;
	}



	public static final String AUTH_TYPE_REGULAR = "REGULAR";
	public static final int AUTH_STATUS_EXISTING = 0;
	public static final int AUTH_STATUS_NEW = 1;
	public static final int AUTH_STATUS_NONE = 2;

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getNickName() {
		return nickName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Boolean getGender() {
		return gender;
	}

	public long getImageId() {
		return imageId;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public Boolean getMarried() {
		return married;
	}

	public Date getMarriedDate() {
		return marriedDate;
	}

	public long getFamilyCodeId() {
		return familyCodeId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public void setImageId(long imageId) {
		this.imageId = imageId;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setMarried(Boolean married) {
		this.married = married;
	}

	public void setMarriedDate(Date marriedDate) {
		this.marriedDate = marriedDate;
	}

	public void setFamilyCodeId(long familyCodeId) {
		this.familyCodeId = familyCodeId;
	}
	
	

	public File getFile() {
		return file;
	}



	public void setFile(File file) {
		this.file = file;
	}



	public FamilyCode getFamilyCode() {
		return familyCode;
	}



	public void setFamilyCode(FamilyCode familyCode) {
		this.familyCode = familyCode;
	}



	@Override
	public int getObjectType() {

		return ObjectTypes.USER;
	}

}
