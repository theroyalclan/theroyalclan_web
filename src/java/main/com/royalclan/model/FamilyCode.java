package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "FAMILY_CODE")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class FamilyCode extends AbstractObject {
	
	public final static String LABEL_CODE="code";
	public final static String LABEL_AVATAR_FILE_ID="avatarFileId";

	@Column(name = "CODE")
	private String code;

	@Column(name = "AVATAR_FILE_ID")
	private long avatarFileId;
	
	@Transient
	private File file;

	public String getCode() {
		return code;
	}

	public long getAvatarFileId() {
		return avatarFileId;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setAvatarFileId(long avatarFileId) {
		this.avatarFileId = avatarFileId;
	}
	
	

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.FAMILY_CODE;
	}

}
