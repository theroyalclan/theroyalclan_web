package com.royalclan.model;

/**
 * List of object types in the system
 * 
 * @author
 * 
 */
public interface ObjectTypes {

	public static final int USER = 101;
	public static final int FILE = 102;
	public static final int FAMILY_CODE = 103;
	public static final int FAMILY_MEMBER = 104;
	public static final int RELATION = 105;
	public static final int EVENT = 106;
	public static final int EVENT_TYPE = 107;
	public static final int TEMPLATE = 108;
	public static final int QUOTE = 109;
	public static final int WISH = 110;

}