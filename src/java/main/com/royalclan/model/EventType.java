package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "EVENT_TYPE")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class EventType extends AbstractObject {
	
	public final static String LABEL_TYPE="type";

	@Column(name = "TYPE")
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.EVENT_TYPE;
	}

}
