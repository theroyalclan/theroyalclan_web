package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "QUOTE")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class Quote extends AbstractObject {

	public final static String LABEL_QUOTE_TEXT="text";
	
	@Column(name = "QUOTE_TEXT")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.QUOTE;
	}

}
