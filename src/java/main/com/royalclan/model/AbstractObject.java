package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonMethod;

/**
 * Abstract Object is the implementation for common attributes of all objects
 * in the system. And also all model objects in the system will inherit this class.
 * @author
 * 
 **/
@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonAutoDetect(value=JsonMethod.SETTER)
public abstract class AbstractObject implements BaseObject{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5722114332231386223L;
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private long id;
	@Column(name = "CTS")
	private long cts;
	@Column(name = "MTS")
	private long mts;
	@Column(name = "CREATED_BY")
	private long creatorId;
	@Column(name = "MODIFIED_BY")
	private long modifierId;
	@Column(name = "DELETED")
	private boolean deleted;

	public static final String LABEL_ID = "id";
	public static final String LABEL_CREATED_TIME = "cts";
	public static final String LABEL_CREATOR_ID = "creatorId";
	public static final String LABEL_MODIFIED_TIME = "mts";
	public static final String LABEL_MODIFIER_ID = "modifierId";
	public static final String LABEL_IS_DELETED = "deleted";

	public AbstractObject() {
		super();
	}

	public AbstractObject(AbstractObject abstractObject) {
		this.id = abstractObject.id;
		this.cts = abstractObject.cts;
		this.creatorId = abstractObject.creatorId;
		this.modifierId = abstractObject.modifierId;
		this.mts = abstractObject.mts;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

//	@JsonIgnore
	@Override
	public long getCts() {
		return cts;
	}

	@Override
	public void setCts(long cts) {
		this.cts = cts;
	}

//	@JsonIgnore
	@Override
	public long getCreatorId() {
		return creatorId;
	}

	@Override
	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}

//	@JsonIgnore
	@Override
	public long getMts() {
		return mts;
	}

	@Override
	public void setMts(long mts) {
		this.mts = mts;
	}

//	@JsonIgnore
	@Override
	public long getModifierId() {
		return modifierId;
	}

	@Override
	public void setModifierId(long modifierId) {
		this.modifierId = modifierId;
	}

//	@JsonIgnore
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
}