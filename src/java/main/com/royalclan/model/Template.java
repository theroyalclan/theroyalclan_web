package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "TEMPLATE")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class Template extends AbstractObject {

	public final static String LABEL_FILE_ID="fileId";
	public final static String LABEL_EVENT_TYPE_ID="eventTypeId";
	
	@Column(name = "FILE_ID")
	private long fileId;

	@Column(name = "EVENT_TYPE_ID")
	private long eventTypeId;
	
	@Transient
	private File file;
	
	@Transient
	private EventType eventType;

	public long getFileId() {
		return fileId;
	}

	public long getEventTypeId() {
		return eventTypeId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public void setEventTypeId(long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.TEMPLATE;
	}

}
