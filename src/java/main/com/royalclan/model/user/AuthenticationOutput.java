package com.royalclan.model.user;

import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonProperty;

import com.royalclan.model.User;
import com.royalclan.service.annotations.SerializationDescriptor;

/**
 * @author 
 * 
 */
public class AuthenticationOutput {

 private String sessionToken;
 private int authStatus;
 private User user;
 ///////////////////////////////


 
 /////////////////////////////
 public AuthenticationOutput(String sessionToken, int authStatus, User user) {
  this.sessionToken = sessionToken;
  this.authStatus = authStatus;
  this.user = user;

 }

 public String getSessionToken() {
  return sessionToken;
 }

 public int getAuthStatus() {
  return authStatus;
 }
 
 public User getUser() {
  return user;
 }
}