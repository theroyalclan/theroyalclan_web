package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "WISH")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class Wish extends AbstractObject {

	public final static String LABEL_MESSAGE="message";
	public final static String LABEL_RECEIVER_ID="receiverId";
	public final static String LABEL_SENDER_ID="senderId";
	public final static String LABEL_FILE_ID="fileId";
	
	@Column(name = "MESSAGE")
	private String message;
	@Column(name = "RECEIVER_ID")
	private long receiverId;
	@Column(name = "SENDER_ID")
	private long senderId;
	@Column(name = "FILE_ID")
	private long fileId;

	
	
	
	public String getMessage() {
		return message;
	}




	public void setMessage(String message) {
		this.message = message;
	}




	public long getReceiverId() {
		return receiverId;
	}




	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}




	public long getSenderId() {
		return senderId;
	}




	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}




	public long getFileId() {
		return fileId;
	}




	public void setFileId(long fileId) {
		this.fileId = fileId;
	}




	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.QUOTE;
	}

}
