package com.royalclan.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "FAMILY_MEMBER")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class FamilyMember extends AbstractObject {
	
	public final static String LABEL_FILE_ID="name";
	public final static String LABEL_NICK_NAME="nickName";
	public final static String LABEL_DOB="dateOfBirth";
	public final static String LABEL_RELATION_ID="relationId";
	public final static String LABEL_IMAGE_ID="imageId";
	public final static String LABEL_USER_ID="userId";

	@Column(name = "NAME")
	private String name;

	@Column(name = "NICK_NAME")
	private String nickName;

	@Column(name = "DOB")
	private Date dateOfBirth;

	@Column(name = "RELATION_ID")
	private long relationId;

	@Column(name = "IMAGE_ID")
	private long imageId;
	
	@Column(name = "USER_ID")
	private long userId;
	
	@Transient
	private Relation relation;
	@Transient
	private File file;

	public String getName() {
		return name;
	}

	public String getNickName() {
		return nickName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public long getRelationId() {
		return relationId;
	}

	public long getImageId() {
		return imageId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setRelationId(long relationID) {
		this.relationId = relationID;
	}

	public void setImageId(long imageId) {
		this.imageId = imageId;
	}
	
	

	public Relation getRelation() {
		return relation;
	}

	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.FAMILY_MEMBER;
	}

}
