package com.royalclan.model;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "EVENT")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class Event extends AbstractObject {
	
	public final static String LABEL_FILE_ID="fileId";
	public final static String LABEL_TYPE_ID="typeId";
	public final static String LABEL_DATE_OF_EVENT="dateOfEvent";
	public final static String LABEL_TIME_OF_EVENT="timeOfEvent";
	public final static String LABEL_LOCATION="location";
	public final static String LABEL_DESCRIPTION="description";
	public final static String LABEL_OWNER_ID="ownerId";

	@Column(name = "FILE_ID")
	private long fileId;

	@Column(name = "TYPE_ID")
	private long typeId;

	@Column(name = "EVENT_DATE")
	private Date dateOfEvent;
	
	@Column(name = "EVENT_TIME")
	private Time timeOfEvent;

	@Column(name = "LOCATION")
	private String location;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "OWNER_ID")
	private long ownerId;
	
	@Transient
	private File file;
	@Transient
	private EventType type;
	@Transient
	private User owner;

	public long getFileId() {
		return fileId;
	}

	public long getTypeId() {
		return typeId;
	}

	public Date getDateOfEvent() {
		return dateOfEvent;
	}

	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public void setDateOfEvent(Date dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	
	

	public Time getTimeOfEvent() {
		return timeOfEvent;
	}

	public void setTimeOfEvent(Time timeOfEvent) {
		this.timeOfEvent = timeOfEvent;
	}
	
	

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.EVENT;
	}

}
