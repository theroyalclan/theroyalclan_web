package com.royalclan.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonMethod;

/**
 * 
 * Defines all the common attributes across all model objects
 * @author 
 *
 * */
@JsonIgnoreProperties(ignoreUnknown=true)
//@JsonAutoDetect(value=JsonMethod.SETTER)
public interface BaseObject extends Serializable {
	

	long getId();

	void setId(long id);
	
	/**
	 * CTS --> creation time stamp
	 * @return
	 */
	long getCts();

	void setCts(long cts);

	long getCreatorId();

	void setCreatorId(long creatorId);

	/**
	 * MTS --> last modified time stamp
	 * @return
	 */	
	long getMts();

	void setMts(long mts);

	long getModifierId();

	void setModifierId(long modifierId);

	/**
	 * Gets the type of object. The references for this is found in 
	 * Object types are defined in ObjectType interface
	 * @return
	 */
//	@JsonIgnore
	int getObjectType();

}
