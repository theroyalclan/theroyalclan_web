package com.royalclan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;

@Entity
@Table(name = "FILE")
@JsonAutoDetect(value=JsonMethod.SETTER)
public class File extends AbstractObject {
	
	public final static String LABEL_FILE_NAME="filaName";
	public final static String LABEL_PATH="path";
	public final static String LABEL_BASE_PATH="basePath";
	public final static String LABEL_DOMAIN_PATH="domainPath";
	public final static String LABEL_HOST_PATH="hostPath";

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "PATH")
	private String path;

	@Column(name = "BASE_PATH")
	private String basePath;

	@Column(name = "DOMAIN_PATH")
	private String domainPath;

	@Column(name = "HOST_PATH")
	private String hostPath;

	public String getFileName() {
		return fileName;
	}

	public String getPath() {
		return path;
	}

	public String getBasePath() {
		return basePath;
	}

	public String getDomainPath() {
		return domainPath;
	}

	public String getHostPath() {
		return hostPath;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public void setDomainPath(String domainPath) {
		this.domainPath = domainPath;
	}

	public void setHostPath(String hostPath) {
		this.hostPath = hostPath;
	}

	@Override
	public int getObjectType() {
		// TODO Auto-generated method stub
		return ObjectTypes.FILE;
	}

}
