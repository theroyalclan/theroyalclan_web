package com.royalclan.handler.user;

import com.royalclan.model.User;

/**
 * @author 
 * 
 */
public class AuthenticationHandlerFactory {

	public static AuthenticationHandler getAuthenticationHandler(String authType) {

		if (authType.equals(User.AUTH_TYPE_REGULAR)) {
			return new DefaultAuthenticationHandler();
		}
		return new DefaultAuthenticationHandler();
	}
}
