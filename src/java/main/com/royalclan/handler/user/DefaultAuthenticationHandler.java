package com.royalclan.handler.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.TimeZone;

import com.royalclan.common.EncryptionFactory;
import com.royalclan.common.Utils;
import com.royalclan.common.cache.Cache;
import com.royalclan.common.cache.CacheManager;
import com.royalclan.common.cache.CacheRegionType;
import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ExceptionCodes;
import com.royalclan.exception.ExceptionMessages;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.exception.SystemException;
import com.royalclan.model.User;
import com.royalclan.model.UserSessionToken;
import com.royalclan.model.user.AuthenticationInput;
import com.royalclan.model.user.AuthenticationOutput;
import com.royalclan.model.user.DefaultAuthenticationInput;
import com.royalclan.persistence.dao.DAOFactory;

/**
 * @author 
 * 
 */
public class DefaultAuthenticationHandler implements AuthenticationHandler {

	@Override
	public AuthenticationOutput authenticate(AuthenticationInput input)
			throws ObjectNotFoundException, BusinessException,
			EncryptionException {

		DefaultAuthenticationInput daInput = (DefaultAuthenticationInput) input;
		int authStatus = User.AUTH_STATUS_NONE;
		String email = daInput.getEmail();
		String password = daInput.getPassword();
		if (email != null) {
			email = email.trim().toLowerCase();
		}
		if (password != null) {
			password = password.trim();
		}
		User user = null;
		DAOFactory daoFactory = DAOFactory.getInstance();
		// TODO handle email doesn't exists case.
		user = daoFactory.getUserDAO().getUserByEmail(email);
		String encryptedPassword = Utils.encrypt(password);
		String passwordFromDB = user.getPassword();
		
		
		/////////////////////////////////////////////////////////////////////////////////
		long userId = user.getId();

		
		
	////////////////////////////////////////////////////////////
		
		boolean userValidity = passwordFromDB.equals(encryptedPassword);

		if (!userValidity) {
			throw new BusinessException(ExceptionCodes.INVALID_PASSWORD,

					ExceptionMessages.INVALID_PASSWORD);
		}//anil

		authStatus = User.AUTH_STATUS_EXISTING;

		String sessionToken = null;
		try {
			TimeZone.setDefault(TimeZone.getDefault());
			sessionToken = URLEncoder
					.encode(EncryptionFactory.getEncryption(true).encrypt(
							user.getEmail()
									+ Calendar.getInstance().getTimeInMillis()),
							"UTF-8");
		} catch (EncryptionException ee) {
			throw new SystemException(ExceptionCodes.INTERNAL_ERROR,
					ExceptionMessages.INTERNAL_ERROR);
		} catch (UnsupportedEncodingException uee) {
			throw new SystemException(ExceptionCodes.INTERNAL_ERROR,
					ExceptionMessages.INTERNAL_ERROR);
		}

		UserSessionToken userSessionToken = new UserSessionToken();
		userSessionToken.setUserEmail(user.getEmail());
		userSessionToken.setUserId(user.getId());
		userSessionToken.setUserSessionId(sessionToken);
		//////////////////////////////////////////

		////////////////////////////////////////////
		Cache cache = CacheManager.getInstance().getCache(CacheRegionType.USER_SESSION_CACHE);
		cache.put(sessionToken, userSessionToken);
		System.out.println("Session Token : "+sessionToken);		
		System.out.println("Cached : "+cache.getValue(sessionToken));
		AuthenticationOutput authenticationOutput = new AuthenticationOutput(sessionToken, authStatus, user);
		return authenticationOutput;
	}

}
