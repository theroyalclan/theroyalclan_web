package com.royalclan.handler.user;

import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.user.AuthenticationInput;
import com.royalclan.model.user.AuthenticationOutput;

/**
 * @author 
 * 
 */
public interface AuthenticationHandler {
	public AuthenticationOutput authenticate(AuthenticationInput input)
			throws ObjectNotFoundException, BusinessException, EncryptionException;
}
