package com.royalclan.handler;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Wish;
import com.royalclan.model.Wish;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;

public class WishHandler extends AbstractHandler {

	private static WishHandler INSTANCE = null;

	private WishHandler() {
	}

	public static WishHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new WishHandler();
		return INSTANCE;
	}

	public Paginator<Wish> getWishes(PaginationInput paginationInput) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getWishDAO().getWishes(paginationInput);
	}
	
	public Wish addWish(Wish wish) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return (Wish) DAOFactory.getInstance().getWishDAO().saveObject(wish);
	}

	public Wish getWish(long eventId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return (Wish) DAOFactory.getInstance().getWishDAO().getObjectById(eventId, ObjectTypes.WISH);
	}

}
