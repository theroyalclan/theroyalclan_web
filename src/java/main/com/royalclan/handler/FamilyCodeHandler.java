package com.royalclan.handler;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.FamilyCode;
import com.royalclan.model.ObjectTypes;
import com.royalclan.persistence.dao.DAOFactory;

public class FamilyCodeHandler extends AbstractHandler {

	private static FamilyCodeHandler INSTANCE = null;

	private FamilyCodeHandler() {
	}

	public static FamilyCodeHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new FamilyCodeHandler();
		return INSTANCE;
	}
	
	public FamilyCode getFamilyCode(long fileCodeId) throws ObjectNotFoundException{
		
		FamilyCode familyCode = (FamilyCode) DAOFactory.getInstance().getFamilyCodeDAO().getObjectById(fileCodeId, ObjectTypes.FAMILY_CODE);
		if(familyCode.getAvatarFileId()!=0){
			try{
				familyCode.setFile(FileHandler.getInstance().getFile(familyCode.getAvatarFileId()));
			}catch(ObjectNotFoundException objectNotFoundException){
				
			}
		}
		
		return familyCode;
	}

}
