package com.royalclan.handler;

import java.util.List;
import java.util.Set;

import com.royalclan.model.EventType;
import com.royalclan.persistence.dao.DAOFactory;

public class EventTypeHandler extends AbstractHandler {

	private static EventTypeHandler INSTANCE = null;

	private EventTypeHandler() {
	}

	public static EventTypeHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new EventTypeHandler();
		return INSTANCE;
	}

	public List<EventType> getEventTypes(Set<Long> fileIds) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getEventTypeDAO().getEventTypes(fileIds);
	}

}
