package com.royalclan.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.royalclan.model.EventType;
import com.royalclan.model.File;
import com.royalclan.model.Template;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.PaginationOutput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;

public class TemplateHandler extends AbstractHandler {

	private static TemplateHandler INSTANCE = null;

	private TemplateHandler() {
	}

	public static TemplateHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new TemplateHandler();
		return INSTANCE;
	}

	public PaginationOutput<Template> getTemplates(PaginationInput paginationInput) {
		// TODO Auto-generated method stub
		
		Paginator<Template> templatesPaginator = DAOFactory.getInstance().getTemplateDAO().getTemplates(paginationInput);
		
		
		Paginator<Template> finalResult = null;
		long count = templatesPaginator.getCount();
		
		List<Template> templates = templatesPaginator.getList();
		Set<Long> fileIds = new HashSet<>();
		Map<Long,File> fileMap = new HashMap();
		
		Set<Long> eventTypeIds = new HashSet();
		Map<Long, EventType> eventTypeMap = new HashMap();
		
		if(CollectionUtils.isNotEmpty(templates)){
			for(Template template:templates){
				if(template.getFileId()!=0)
					fileIds.add(template.getFileId());
				if(template.getEventTypeId()!=0)
					eventTypeIds.add(template.getEventTypeId());
			}

			List<File> files = null;
			List<EventType> eventTypes = null;
			if(CollectionUtils.isNotEmpty(fileIds))
				files = FileHandler.getInstance().getFiles(fileIds);
			
			if(CollectionUtils.isNotEmpty(eventTypeIds))
				eventTypes = EventTypeHandler.getInstance().getEventTypes(fileIds);
			
			if(CollectionUtils.isNotEmpty(files)){
				for(File file : files){
					fileMap.put(file.getId(), file);
				}
			}
			
			if(CollectionUtils.isNotEmpty(eventTypes)){
				for(EventType file : eventTypes){
					eventTypeMap.put(file.getId(), file);
				}
			}
			
			List<Template> updatedTemplates = new ArrayList<>();
			for(Template user:templates){
				if(user.getFileId() !=0)
					user.setFile(fileMap.get(user.getFileId()));
				if(user.getEventTypeId()!=0)
					user.setEventType(eventTypeMap.get(user.getEventTypeId()));
				updatedTemplates.add(user);
			}
			finalResult = new Paginator<>(updatedTemplates, count);
		}
		
		
		
		return new PaginationOutput<>(finalResult, paginationInput.getPageNo(), paginationInput.getPageSize());
	}
}
