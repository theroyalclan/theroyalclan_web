package com.royalclan.handler;

import com.royalclan.model.Quote;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;

public class QuoteHandler extends AbstractHandler {

	private static QuoteHandler INSTANCE = null;

	private QuoteHandler() {
	}

	public static QuoteHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new QuoteHandler();
		return INSTANCE;
	}

	public Paginator<Quote> getQuotes(PaginationInput paginationInput) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getQuoteDAO().getQuotes(paginationInput);
	}

}
