package com.royalclan.handler;

import java.util.List;
import java.util.Set;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Relation;
import com.royalclan.persistence.dao.DAOFactory;

public class RelationHandler extends AbstractHandler {

	private static RelationHandler INSTANCE = null;

	private RelationHandler() {
	}

	public static RelationHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new RelationHandler();
		return INSTANCE;
	}

	public List<Relation> getAllRelations() {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getRelationDAO().getAllRelations();
	}
	
	public List<Relation> getRelations(Set<Long> relationIds) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getRelationDAO().getRelations(relationIds);
	}

	public Relation getRelation(long relationId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
			return (Relation) DAOFactory.getInstance().getRelationDAO().getObjectById(relationId, ObjectTypes.RELATION);
	}

}
