package com.royalclan.handler;

import com.royalclan.model.Event;
import com.royalclan.model.EventType;
import com.royalclan.model.FamilyCode;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.File;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Quote;
import com.royalclan.model.Relation;
import com.royalclan.model.Template;
import com.royalclan.model.User;

/**
 * The base class for all handlers
 * 
 * @author
 *
 */
public class AbstractHandler {
	public static final int ALL_CATEGORIES = -1;
	
	private Class getClassFromObjectType(int objectType){
		
		Class className = null;
		switch(objectType){
		case ObjectTypes.EVENT:
			className = Event.class;
			break;
		case ObjectTypes.EVENT_TYPE:
			className = EventType.class;
			break;
		case ObjectTypes.FAMILY_CODE:
			className = FamilyCode.class;
			break;
		case ObjectTypes.FAMILY_MEMBER:
			className = FamilyMember.class;
			break;
		case ObjectTypes.FILE:
			className = File.class;
			break;
		case ObjectTypes.QUOTE:
			className = Quote.class;
			break;
		case ObjectTypes.RELATION:
			className = Relation.class;
			break;
		case ObjectTypes.TEMPLATE:
			className = Template.class;
			break;
			
		case ObjectTypes.USER:
			className = User.class;
			break;
		default:
			break;
		}
		return className;
}
}
