package com.royalclan.handler;

import java.util.List;
import java.util.Set;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.File;
import com.royalclan.model.ObjectTypes;
import com.royalclan.persistence.dao.DAOFactory;

public class FileHandler extends AbstractHandler {

	private static FileHandler INSTANCE = null;

	private FileHandler() {
	}

	public static FileHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new FileHandler();
		return INSTANCE;
	}
	
	public File getFile(long fileId) throws ObjectNotFoundException{
		
		return (File) DAOFactory.getInstance().getFileDAO().getObjectById(fileId, ObjectTypes.FILE);
	}
	
	public File saveFile(File file) {
    	File fileSaved = (File) DAOFactory.getInstance().getFileDAO().saveObject(file);
		return fileSaved;
}

	public List<File> getFiles(Set<Long> fileIds) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getFileDAO().getFiles(fileIds);
	}

}
