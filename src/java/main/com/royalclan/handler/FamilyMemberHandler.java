package com.royalclan.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.File;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Relation;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.PaginationOutput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;

public class FamilyMemberHandler extends AbstractHandler {

	private static FamilyMemberHandler INSTANCE = null;

	private FamilyMemberHandler() {
	}

	public static FamilyMemberHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new FamilyMemberHandler();
		return INSTANCE;
	}

	public PaginationOutput<FamilyMember> getFamilyMembers(PaginationInput input,long userId) {
		
		
		// TODO Auto-generated method stub
		Paginator<FamilyMember> finalResult = null;
		Paginator<FamilyMember> templatesPaginator = DAOFactory.getInstance().getFamilyMemberDAO().getFamilyMembers(input, userId);
		long count = templatesPaginator.getCount();
		List<FamilyMember> templates = templatesPaginator.getList();
		Set<Long> fileIds = new HashSet<>();
		Set<Long> relationIds = new HashSet<>();
		Map<Long,File> fileMap = new HashMap();
		Map<Long,Relation> relationMap = new HashMap();
		if(CollectionUtils.isNotEmpty(templates)){
			for(FamilyMember template:templates){
				if(template.getImageId()!=0)
					fileIds.add(template.getImageId());
				if(template.getRelationId()!=0)
					relationIds.add(template.getRelationId());
			}

			List<File> files = null;
			List<Relation> relations = null;
			if(CollectionUtils.isNotEmpty(fileIds))
				files = FileHandler.getInstance().getFiles(fileIds);
			if(CollectionUtils.isNotEmpty(relationIds))
				relations = RelationHandler.getInstance().getRelations(relationIds);
			if(CollectionUtils.isNotEmpty(files)){
				for(File file : files){
					fileMap.put(file.getId(), file);
				}
			}
			
			if(CollectionUtils.isNotEmpty(relations)){
				for(Relation relation : relations){
					relationMap.put(relation.getId(), relation);
				}
			}
			List<FamilyMember> updatedUsers = new ArrayList<>();
			for(FamilyMember user:templates){
				if(user.getImageId() !=0)
					user.setFile(fileMap.get(user.getImageId()));
				if(user.getRelationId()!=0)
					user.setRelation(relationMap.get(user.getRelationId()));
				updatedUsers.add(user);
			}
			finalResult = new Paginator<>(updatedUsers, count);
		}
		return new PaginationOutput<FamilyMember>(finalResult, input.getPageNo(), input.getPageSize());
		
	}
	
	public FamilyMember addFamilyMember(FamilyMember familyMember){
		return (FamilyMember) DAOFactory.getInstance().getFamilyMemberDAO().saveObject(familyMember);
	}
	
	public FamilyMember getFamilyMember(long id) throws ObjectNotFoundException{
		
		FamilyMember familyMember = (FamilyMember) DAOFactory.getInstance().getFamilyMemberDAO().getObjectById(id, ObjectTypes.FAMILY_MEMBER);
		if(familyMember.getImageId()!=0)
			familyMember.setFile(FileHandler.getInstance().getFile(familyMember.getImageId()));
		if(familyMember.getRelationId()!=0)
			familyMember.setRelation(RelationHandler.getInstance().getRelation(familyMember.getRelationId()));
		return familyMember;
	}
	
	
	public FamilyMember updateFamilyMember(FamilyMember familyMember) throws ObjectNotFoundException{
		FamilyMember memberDB = (FamilyMember) DAOFactory.getInstance().getFamilyMemberDAO().getObjectById(familyMember.getId(), ObjectTypes.FAMILY_MEMBER);
		
		memberDB.setDateOfBirth(familyMember.getDateOfBirth());
		memberDB.setImageId(familyMember.getImageId());
		memberDB.setName(familyMember.getName());
		memberDB.setNickName(familyMember.getNickName());
		memberDB.setRelationId(familyMember.getRelationId());
		
		return (FamilyMember) DAOFactory.getInstance().getFamilyMemberDAO().update(memberDB);
	}
	
	public boolean deleteFamilyMember(long id){
		return DAOFactory.getInstance().getFamilyMemberDAO().deleteObjectById(id, ObjectTypes.FAMILY_MEMBER);
	}


}
