package com.royalclan.handler;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.Event;
import com.royalclan.model.ObjectTypes;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;
import com.royalclan.service.descriptors.EventPaginationInput;

public class EventHandler extends AbstractHandler {

	private static EventHandler INSTANCE = null;

	private EventHandler() {
	}

	public static EventHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new EventHandler();
		return INSTANCE;
	}
	
	public Paginator<Event> getEvents(EventPaginationInput eventPaginationInput){
		

		return DAOFactory.getInstance().getEventDAO().getEvents(eventPaginationInput);
	}

	public Event getEvent(long eventId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return (Event) DAOFactory.getInstance().getEventDAO().getObjectById(eventId, ObjectTypes.EVENT);
	}
	
	public Event addEvent(Event event) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return (Event) DAOFactory.getInstance().getEventDAO().saveObject(event);
	}
	
	public Event updateEvent(Event event) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		
		Event eventFromDB = (Event) DAOFactory.getInstance().getEventDAO().getObjectById(event.getId(), ObjectTypes.EVENT);
		eventFromDB.setDateOfEvent(event.getDateOfEvent());
		eventFromDB.setDescription(event.getDescription());
		eventFromDB.setFileId(event.getFileId());
		eventFromDB.setLocation(event.getLocation());
		eventFromDB.setOwnerId(event.getOwnerId());
		eventFromDB.setTimeOfEvent(event.getTimeOfEvent());
		eventFromDB.setTypeId(event.getTypeId());
		
		return (Event) DAOFactory.getInstance().getEventDAO().update(event);
	}
	
	public boolean deleteEvent(long eventId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return  DAOFactory.getInstance().getEventDAO().deleteObjectById(eventId, ObjectTypes.EVENT);
	}

}
