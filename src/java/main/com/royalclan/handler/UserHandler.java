package com.royalclan.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.mail.EmailException;

import com.royalclan.common.RandomPasswordGenerator;
import com.royalclan.common.Utils;
import com.royalclan.common.cache.CacheManager;
import com.royalclan.common.cache.CacheRegionType;
import com.royalclan.exception.BusinessException;
import com.royalclan.exception.EncryptionException;
import com.royalclan.exception.ExceptionCodes;
import com.royalclan.exception.ExceptionMessages;
import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.exception.UserException;
import com.royalclan.handler.user.AuthenticationHandlerFactory;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.File;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.User;
import com.royalclan.model.user.AuthenticationOutput;
import com.royalclan.model.user.DefaultAuthenticationInput;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.PaginationOutput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.DAOFactory;
import com.royalclan.persistence.dao.UserDAO;
import com.royalclan.service.common.ServiceRequestContextHolder;
import com.royalclan.service.descriptors.ChangePassword;

/**
 * @author
 *
 */

public class UserHandler extends AbstractHandler {
	private static UserHandler INSTANCE = null;

	private UserHandler() {
	}

	/**
	 * @return instance of UserHandler
	 */
	public static UserHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new UserHandler();
		return INSTANCE;
	}

	public User getUserByEmail(String email) throws UserException {
		User user = null;
		UserDAO userDAOImpl = DAOFactory.getInstance().getUserDAO();
		user = userDAOImpl.getUserByEmail(email);

		return user;
	}

	public AuthenticationOutput register(User user) throws EncryptionException, ObjectNotFoundException, BusinessException{
		String password = user.getPassword();
		User output = save(user);
		DefaultAuthenticationInput input = new DefaultAuthenticationInput();
		input.setEmail(user.getEmail());
		input.setPassword(password);
		AuthenticationOutput authOutput = AuthenticationHandlerFactory.getAuthenticationHandler(input.getAuthType())
				.authenticate(input);
		return authOutput;
	}

	private User save(User user) throws EncryptionException, BusinessException {
		User userSaved = null;
		if(validateUser(user)){
			if(user.getMarriedDate() !=null)
				user.setMarried(true);
			else
				user.setMarried(false);
			user.setGender(true);

			user.setPassword(Utils.encrypt(user.getPassword()));
			userSaved  = (User) DAOFactory.getInstance().getUserDAO().saveObject(user);

		}
		return userSaved;
	}

	private boolean validateUser(User user) throws BusinessException {
		// TODO Auto-generated method stub
		validateEmail(user);
		return true;
	}

	private boolean validateEmail(User user) throws BusinessException {
		// TODO Auto-generated method stub
		if(user.getEmail().isEmpty() | user.getEmail().trim().length()==0){
			throw new BusinessException();
		}
		if(isDuplicateEmail(user)){
			throw new BusinessException();
		}
		if(user.getId()==0){
			if(user.getPassword().isEmpty() | user.getPassword().trim().length()==0)
				throw new BusinessException();
		}
		return true;
	}

	private boolean isDuplicateEmail(User user) {
		// TODO Auto-generated method stub
		return DAOFactory.getInstance().getUserDAO().isDuplicateEmail(user);
	}

	public User getLoggedInUser(long userId) {
		// TODO Auto-generated method stub
		User user = null;
		UserDAO userDAOImpl = (UserDAO) DAOFactory.getInstance().getUserDAO();
		user = (User) userDAOImpl.getLoggedInUser(userId);
		if(user.getImageId() !=0){
			try{
				user.setFile(FileHandler.getInstance().getFile(user.getImageId()));
			}
			catch(ObjectNotFoundException exception){

			}
		}
		if(user.getFamilyCodeId()!=0){
			try{
				user.setFamilyCode(FamilyCodeHandler.getInstance().getFamilyCode(user.getFamilyCodeId()));
			}
			catch(ObjectNotFoundException objectNotFoundException){

			}
		}

		return user;	
	}

	public boolean forgotPassword(String email) throws EncryptionException, BusinessException  {
		User userFromDB=null;
		User user=(User)DAOFactory.getInstance().getUserDAO().getUserByEmail(email);
		if (email == null || email.isEmpty() || email.trim().isEmpty()) {
			throw new BusinessException(ExceptionCodes.EMAIL_CANNOT_BE_EMPTY,
					ExceptionMessages.EMAIL_CANNOT_BE_EMPTY);
		}
		else if(user == null){
			//			throw new EmployeeException(ExceptionCodes.PLEASE_ENTER_THE_EMAIL_WHICH_YOU_HAVE_GIVEN_AT_THE_TIME_OF_REGISTRATION,
			//					ExceptionMessages.PLEASE_ENTER_THE_EMAIL_WHICH_YOU_HAVE_GIVEN_AT_THE_TIME_OF_REGISTRATION);	
		}
		else{
			String randomPassword=new String(PasswordGenereation());
			user.setPassword(Utils.encrypt(randomPassword));
			UserDAO userDAO = (UserDAO) DAOFactory.getInstance().getUserDAO();
			userFromDB=(User) userDAO.update(user);
			try {
				String out= SendNotificationHistoryHandler.getInstance().sendForgotPasswordMail(user.getEmail(),user.getNickName(),randomPassword);
				System.out.println(out);
			} catch (EmailException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public char[] PasswordGenereation(){
		int noOfCAPSAlpha = 1;
		int noOfDigits = 1;
		int noOfSplChars = 0;
		int minLen = 8;
		int maxLen = 12;
		char[] pswd=RandomPasswordGenerator.generatePswd(minLen, maxLen,noOfCAPSAlpha, noOfDigits, noOfSplChars);
		return pswd;
	}


	public void changePassword(ChangePassword changePasswordEmployee)
			throws EncryptionException, BusinessException {
		long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
		User userFromDB = (User) DAOFactory.getInstance().getUserDAO().getObjectById(userId, ObjectTypes.USER);
		String passwordFromDB = userFromDB.getPassword();
		String oldpassword = changePasswordEmployee.getOldPassword();
		String newPassword = changePasswordEmployee.getNewPassword();
		String confirmNewPassword = changePasswordEmployee.getConfirmNewPassword();
		String encryptedOldPassword = Utils.encrypt(oldpassword.trim());
		if (newPassword == null || newPassword.isEmpty()
				|| newPassword.trim().isEmpty() || confirmNewPassword == null
				|| confirmNewPassword.isEmpty()
				|| confirmNewPassword.trim().isEmpty()) {
			throw new BusinessException(
					ExceptionCodes.PASSWORD_CANNOT_BE_EMPTY,
					ExceptionMessages.PASSWORD_CANNOT_BE_EMPTY);
		}
		boolean passwordValidity = passwordFromDB.equals(encryptedOldPassword);
		boolean newPasswordValidity = newPassword.equals(confirmNewPassword);
		if (passwordValidity && newPasswordValidity) {
			userFromDB.setPassword(Utils.encrypt(newPassword));
			updateUser(userFromDB);
		} else if (!passwordValidity) {
			/*			throw new UserException(ExceptionCodes.INVALID_OLD_PASSWORD,
					ExceptionMessages.INVALID_OLD_PASSWORD);*/
		} else if (!newPasswordValidity) {
			/*			throw new UserException(ExceptionCodes.INVALID_NEW_PASSWORD,
					ExceptionMessages.INVALID_NEW_PASSWORD);*/
		}
	}

	public User updateUser(User user)
			throws EncryptionException, BusinessException {

		
		User userFromDB = (User) DAOFactory.getInstance().getUserDAO().getObjectById(user.getId(), ObjectTypes.USER);
		if(user.getFirstName()!=null)
			userFromDB.setFirstName(user.getFirstName());
		if(user.getLastName()!=null)
			userFromDB.setLastName(user.getLastName());
		if(user.getNickName()!=null)
			userFromDB.setNickName(user.getNickName());
		if(user.getImageId()!=0)
			userFromDB.setImageId(user.getImageId());
		if(user.getDateOfBirth()!=null)
			userFromDB.setDateOfBirth(user.getDateOfBirth());
		if(user.getMarriedDate()!=null){
			userFromDB.setMarriedDate(user.getMarriedDate());
			userFromDB.setMarried(true);
		}
		if(user.getMarried()==null){
			userFromDB.setMarriedDate(null);
			userFromDB.setMarried(false);
		}
		
		User userUpdated = (User) DAOFactory.getInstance().getUserDAO().update(userFromDB);
		
		if(userUpdated.getImageId() !=0){
			try{
				userUpdated.setFile(FileHandler.getInstance().getFile(userUpdated.getImageId()));
			}
			catch(ObjectNotFoundException exception){

			}
		}
		if(userUpdated.getFamilyCodeId()!=0){
			try{
				userUpdated.setFamilyCode(FamilyCodeHandler.getInstance().getFamilyCode(userUpdated.getFamilyCodeId()));
			}
			catch(ObjectNotFoundException objectNotFoundException){

			}
		}


		return userUpdated;
	}

	public boolean logout() {
		boolean isLogout = false;
		String userSessionId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserSessionId();
		isLogout = CacheManager.getInstance().getCache(CacheRegionType.USER_SESSION_CACHE).remove(userSessionId);
		return isLogout;
	}

	public PaginationOutput<FamilyMember> getFamilyMembers(PaginationInput input, long userId) {
		// TODO Auto-generated method stub

		return FamilyMemberHandler.getInstance().getFamilyMembers(input, userId);
	}

	public PaginationOutput<User> getAllUsers(PaginationInput input, long userId) {
		// TODO Auto-generated method stub
		Paginator<User> finalResult = null;
		Paginator<User> usersPaginator = DAOFactory.getInstance().getUserDAO().getUsers(input, userId);
		long count = usersPaginator.getCount();
		List<User> users = usersPaginator.getList();
		Set<Long> fileIds = new HashSet<>();
		Map<Long,File> fileMap = new HashMap();
		if(CollectionUtils.isNotEmpty(users)){
			for(User template:users){
				if(template.getImageId()!=0)
					fileIds.add(template.getImageId());
			}

			List<File> files = null;
			if(CollectionUtils.isNotEmpty(fileIds))
				files = FileHandler.getInstance().getFiles(fileIds);
			if(CollectionUtils.isNotEmpty(files)){
				for(File file : files){
					fileMap.put(file.getId(), file);
				}
			}
			List<User> updatedUsers = new ArrayList<>();
			for(User user:users){
				if(user.getImageId() !=0)
					user.setFile(fileMap.get(user.getImageId()));
				updatedUsers.add(user);
			}
			finalResult = new Paginator<>(updatedUsers, count);
		}
		return new PaginationOutput<User>(finalResult,input.getPageNo(), input.getPageSize());
	}

	public FamilyMember addFamilyMember(FamilyMember familyMember) {
		// TODO Auto-generated method stub
		return FamilyMemberHandler.getInstance().addFamilyMember(familyMember);
	}
	
	public FamilyMember updateFamilyMember(FamilyMember familyMember) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return FamilyMemberHandler.getInstance().updateFamilyMember(familyMember);
	}
	public boolean deletFamilyMember(long id) {
		// TODO Auto-generated method stub
		return FamilyMemberHandler.getInstance().deleteFamilyMember(id);
	}
	
	public FamilyMember getFamilyMember(long familyMemberId) throws ObjectNotFoundException {
		// TODO Auto-generated method stub
		return FamilyMemberHandler.getInstance().getFamilyMember(familyMemberId);
	}


}