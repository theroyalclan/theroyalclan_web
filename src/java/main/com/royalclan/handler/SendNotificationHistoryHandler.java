package com.royalclan.handler;
/**
 * *
 *
 * @author 
 *         
 */

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

import com.royalclan.common.ConfigReader;
import com.royalclan.common.Constants;

public class SendNotificationHistoryHandler extends AbstractHandler {

	private static SendNotificationHistoryHandler INSTANCE = null;
	
	
	private SendNotificationHistoryHandler() {
	}

	/**
	 * TemplateDAO
	 * 
	 * @return instance of UserHandler
	 */
	public static SendNotificationHistoryHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new SendNotificationHistoryHandler();
		return INSTANCE;
	}
	 
	 
	  public String sendRegistrationMail(String employeeEmail,String employeeName,String randomPassword) throws EmailException{
		  

          String hostName=null;
          String smtpPort=null;
          String authenticatorMail=null;
          String authenticatorPassword=null;
          String from=null;
          String bcc=null;
          String URL=null;
          long file_id;
         
/*          try
          {
              Properties props = ConfigReader.getProperties(Constants.MAIL_CONFIGURATION_SETTING);
              hostName=props.getProperty(Constants.HOST_NAME);
           smtpPort=props.getProperty(Constants.HOST_NAME);                                                               
           authenticatorMail=props.getProperty(Constants.AUTHENTICATOR_MAIL);
           authenticatorPassword=props.getProperty(Constants.AUTHENTICATOR_PASSWORD);
           from=props.getProperty(Constants.SEND_FROM);
           bcc=props.getProperty(Constants.SEND_BCC);
           URL=props.getProperty(Constants.URL);                                                            
          }
          catch(IOException e)
          {
              e.printStackTrace();
          }*/
          
          //Templete templte=TemplateHandler.getInstance().viewTemplateAOP(template.getId());
          String content="";
          
          content +="Hi ,    <b>"+employeeName+"</b><br/><br/>";
        		  
        content += "Welcome to <b>Qi People.</b><br/><br/>";
        		   
        content	+= "You have been registered on the system. You can login to the system with following credentials:<br/><br/>";

        content += "<b>URL:</b>"+URL+"<br/><br/>";
        		   
        content += "<b>Username:</b>"+employeeEmail+"<br/><br/>";
        content += "<b>Password:</b>"+randomPassword+"<br/><br/>";
        		   
        content += "Please change your password once you login to the system.<br/><br/>";
        		   
        content += "<b> Features:</b><br/>";
        		content +=  " -          Qi People lets you view some of your basic information<br/>";
        				content +=   "-          Allows you to edit some information and upgrade your skill set<br/>";
        				content += 	 " -          You can view the yearly Holiday list<br/>";
        				content +=  "-          One stop for all HR policies<br/>";
        				content +=  "-          And All Hands data  too<br/><br/><br/>";
        		   
        				content +=  " <b>Thanks,</b><br/><br/>";
        				content +=  "<b>Team HR.</b>";
        		   
        		

		  
		  MultiPartEmail email = new MultiPartEmail();
          email.setHostName(hostName);
          email.setSmtpPort(587);
          email.setAuthenticator(new DefaultAuthenticator(
                  authenticatorMail, authenticatorPassword));
         // email.setSSLOnConnect(true);
          email.setFrom(from);
         // email.setBounceAddress("rahul.shelke@qison.com");
         
         // String subjectMail = sentMailToEmployee.getEvent() + " "
                 // + sentMailToEmployee.getEmployeeName();
         	               
          email.setSubject("HRMS registration");
          email.addTo(employeeEmail);
          email.setContent(content,"text/html");
          email.send();
          return "{\"status\": \"SUCCESS\", \"payload\": \"Mail Send\"}";
		  
		  
	  }

		public String sendForgotPasswordMail(String email, String employeeName,String randomPassword) throws EmailException {
			 String hostName=null;
	          String smtpPort=null;
	          String authenticatorMail=null;
	          String authenticatorPassword=null;
	          String from=null;
	          String bcc=null;
//	          String URL=null;
	          long file_id;
	         
	          try
	          {
	              Properties props = ConfigReader.getProperties(Constants.MAIL_CONFIGURATION_SETTING);
	              hostName=props.getProperty(Constants.HOST_NAME);
	           smtpPort=props.getProperty(Constants.HOST_NAME);                                                               
	           authenticatorMail=props.getProperty(Constants.AUTHENTICATOR_MAIL);
	           authenticatorPassword=props.getProperty(Constants.AUTHENTICATOR_PASSWORD);
	           from=props.getProperty(Constants.SEND_FROM);
	           bcc=props.getProperty(Constants.SEND_BCC);
//	           URL=props.getProperty(Constants.URL);                                                            
	          }
	          catch(IOException e)
	          {
	              e.printStackTrace();
	          }
	          
	          //Templete templte=TemplateHandler.getInstance().viewTemplateAOP(template.getId());
	          String content="";
	          
	          content +="Hi ,    <b>"+employeeName+"</b><br/><br/>";
	          
	        content	+= "You can Login by using following credentials<br/><br/>";
		   
	        content += "<b>Username:</b>"+email+"<br/><br/>";
	        content += "<b>Password:</b>"+randomPassword+"<br/><br/>";
	        		   
	        content += "Please change your password once you login to the system.<br/><br/>";

			  MultiPartEmail mulEmail = new MultiPartEmail();
			  mulEmail.setHostName(hostName);
			  mulEmail.setSmtpPort(587);
			  mulEmail.setAuthenticator(new DefaultAuthenticator(
	                  authenticatorMail, authenticatorPassword));
			  //mulEmail.setSSLOnConnect(true);
			  mulEmail.setFrom(from);
			  mulEmail.setSSLOnConnect(true);
			  mulEmail.setTLS(true);
			  mulEmail.setStartTLSEnabled(true);
	         
	         // String subjectMail = sentMailToEmployee.getEvent() + " "
	                 // + sentMailToEmployee.getEmployeeName();
	         	               
			  mulEmail.setSubject("Forgot Password");
			  mulEmail.addTo(email);
			  mulEmail.setContent(content,"text/html");
			  mulEmail.setBounceAddress("nagashiva18@gmail.com");
			  mulEmail.send();
	          return "{\"status\": \"SUCCESS\", \"payload\": \"Mail Send\"}";
			  
		}
	 
}
