package com.royalclan.persistence.daoimpl;
import static com.royalclan.common.Utils.getDBSession;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.AbstractObject;
import com.royalclan.model.BaseObject;
import com.royalclan.model.Event;
import com.royalclan.model.EventType;
import com.royalclan.model.FamilyCode;
import com.royalclan.model.FamilyMember;
import com.royalclan.model.File;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Quote;
import com.royalclan.model.Relation;
import com.royalclan.model.Template;
import com.royalclan.model.User;
import com.royalclan.persistence.dao.BaseDAO;
import com.royalclan.persistence.session.SessionFactoryUtil;
import com.royalclan.service.common.ServiceRequestContextHolder;

public class BaseDAOImpl implements BaseDAO{
	
	public Session getSession(){
		
		Session session = getDBSession();
		Transaction tx = null;
		try {
			if (null == session) {
				session = SessionFactoryUtil.getInstance().openSession();
				tx = SessionFactoryUtil.getInstance().beginTransaction(session);
			}
		}finally {

			try {
				if (tx != null) {
					tx.commit();
//					if (session.isConnected())
//						session.close();
				}
			} catch (HibernateException e) {

				e.printStackTrace();
			}
		}
		
		
		
		return session;
	}

	@Override
	public BaseObject saveObject(BaseObject persistentObject) {
		Session session = getSession();
		Transaction tx = null;
        if (session == null) { 
        	session = SessionFactoryUtil.getInstance().getCurrentSession();
            tx = SessionFactoryUtil.getInstance().beginTransaction(session);
        }
        try {
            long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
            persistentObject.setCreatorId(userId);
            persistentObject.setModifierId(userId);
        } catch (Exception e) {
        }
		persistentObject.setCts(com.royalclan.common.DateUtils.getCurrentTimeInGMT());
		persistentObject.setMts(com.royalclan.common.DateUtils.getCurrentTimeInGMT());
	    session.save(persistentObject);
        if (tx != null) {
            tx.commit();
        }
		return persistentObject;
	}
	@Override
	 public  BaseObject getObjectById(long id, int objectType)
	   throws ObjectNotFoundException{
		return (BaseObject) getSession().createCriteria(getEntityClass(objectType)).add(Restrictions.eq(AbstractObject.LABEL_ID, id)).uniqueResult();
	}
	 private Class getEntityClass(int objectType) {
		// TODO Auto-generated method stub
		 Class entityClass = null;
		 switch(objectType){
			case ObjectTypes.EVENT:
				entityClass = Event.class;
				break;
			case ObjectTypes.EVENT_TYPE:
				entityClass = EventType.class;
				break;
			case ObjectTypes.FAMILY_CODE:
				entityClass = FamilyCode.class;
				break;
			case ObjectTypes.FAMILY_MEMBER:
				entityClass = FamilyMember.class;
				break;
			case ObjectTypes.FILE:
				entityClass = File.class;
				break;
			case ObjectTypes.QUOTE:
				entityClass = Quote.class;
				break;
			case ObjectTypes.RELATION:
				entityClass = Relation.class;
				break;
			case ObjectTypes.TEMPLATE:
				entityClass = Template.class;
				break;
				
			case ObjectTypes.USER:
				entityClass = User.class;
				break;
			default:
				break;
			}
		return entityClass;
	}

	@Override
	 public BaseObject update(BaseObject persistentObject) {
		 Session session = getSession();
		 Transaction tx = null;
	        if (session == null) { 
	        	session = SessionFactoryUtil.getInstance().getCurrentSession();
	            tx = SessionFactoryUtil.getInstance().beginTransaction(session);
	        }
	        try {
	            long userId = ServiceRequestContextHolder.getContext().getUserSessionToken().getUserId();
	            persistentObject.setModifierId(userId);
	        } catch (Exception e) {
	        }
		 persistentObject.setMts(com.royalclan.common.DateUtils.getCurrentTimeInGMT());
	  session.update(persistentObject);
	  return persistentObject;
	 }

	 @Override
	 public List<BaseObject> save(List<BaseObject> objectList) {
		Session  session = getSession();
	  if (null != objectList && objectList.size() > 0) {
	   short count = 0;
	   for (BaseObject object : objectList) {
	    saveObject(object);
	    count++;
	    if (count == 1000) {// batch update for each 30 records
	     session.flush();
	     session.clear();
	     count = 0;
	    }
	   }
	  }
	  return objectList;
	 }
	 
		@Override
		public long getTotalCount(int objectType) {
			// TODO Auto-generated method stub
			Criteria criteriaCount = getSession().createCriteria(getEntityClass(objectType));
			
			
			criteriaCount.setProjection(Projections.rowCount());
			Long count = (Long) criteriaCount.uniqueResult();
			
			return count;
		}
		
		
	    public Criteria createCriteria(Class inputClass) {
	        return getSession().createCriteria(inputClass);
	    }
	    public Criteria createCustomCriteria(Class inputClass) {
	        Criteria criteria = getDBSession().createCriteria(inputClass);
	        criteria.add(Restrictions.eq(AbstractObject.LABEL_IS_DELETED, Boolean.FALSE));
	        return criteria;
	}

		@Override
		public boolean deleteObjectById(long id, int objectType) {
			// TODO Auto-generated method stub
		    Query query=getSession().createQuery("update "+getEntityClass(objectType).getName()+" set deleted=TRUE where id = "+id);  
			return query.executeUpdate()>0;
		}
}
