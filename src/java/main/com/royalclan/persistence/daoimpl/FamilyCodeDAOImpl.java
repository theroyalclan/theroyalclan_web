package com.royalclan.persistence.daoimpl;

import com.royalclan.persistence.dao.FamilyCodeDAO;

public class FamilyCodeDAOImpl extends BaseDAOImpl implements FamilyCodeDAO {

	private static FamilyCodeDAO INSTANCE = null;

	private FamilyCodeDAOImpl() {
	}

	public static FamilyCodeDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new FamilyCodeDAOImpl();
		}
		return INSTANCE;
	}

}
