package com.royalclan.persistence.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.royalclan.model.FamilyMember;
import com.royalclan.model.ObjectTypes;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.FamilyMemberDAO;

public class FamilyMemberDAOImpl extends BaseDAOImpl implements FamilyMemberDAO {

	private static FamilyMemberDAO INSTANCE = null;

	private FamilyMemberDAOImpl() {
	}

	public static FamilyMemberDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new FamilyMemberDAOImpl();
		}
		return INSTANCE;
	}
	
	public Paginator<FamilyMember> getFamilyMembers(PaginationInput input, long userId) {
		 int pageNo = input.getPageNo();
		  int pageSize = input.getPageSize();
		  
		  int skipCount = (pageNo - 1) * pageSize;  
		  Criteria criteria=createCustomCriteria(FamilyMember.class);
		  criteria.add(Restrictions.eq(FamilyMember.LABEL_USER_ID, userId));
		  criteria.addOrder(Order.asc(FamilyMember.LABEL_NICK_NAME));
		  

		        criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		  List<FamilyMember> familyMembers=criteria.list();
		  
		
		  Long totalCount = getTotalCount(ObjectTypes.FAMILY_MEMBER);

		  Paginator<FamilyMember> familyMembersPaginator = new Paginator<>(familyMembers, totalCount);
return familyMembersPaginator;
	}

}
