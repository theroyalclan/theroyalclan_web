package com.royalclan.persistence.daoimpl;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.royalclan.model.EventType;
import com.royalclan.model.File;
import com.royalclan.persistence.dao.EventTypeDAO;

public class EventTypeDAOImpl extends BaseDAOImpl implements EventTypeDAO {

	private static EventTypeDAO INSTANCE = null;

	private EventTypeDAOImpl() {
	}

	public static EventTypeDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EventTypeDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public List<EventType> getEventTypes(Set<Long> fileIds) {
		// TODO Auto-generated method stub
		
		Criteria criteria = createCustomCriteria(EventType.class);
		criteria.add(Restrictions.in(File.LABEL_ID, fileIds));
		
		
		return criteria.list();
	}

}
