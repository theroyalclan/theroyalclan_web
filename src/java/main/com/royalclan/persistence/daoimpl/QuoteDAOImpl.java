package com.royalclan.persistence.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import com.royalclan.model.FamilyMember;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Quote;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.QuoteDAO;

public class QuoteDAOImpl extends BaseDAOImpl implements QuoteDAO {

	private static QuoteDAO INSTANCE = null;

	private QuoteDAOImpl() {
	}

	public static QuoteDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new QuoteDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public Paginator<Quote> getQuotes(PaginationInput input) {
		int pageNo = input.getPageNo();
		int pageSize = input.getPageSize();

		int skipCount = (pageNo - 1) * pageSize;  
		Criteria criteria=createCustomCriteria(Quote.class);
		criteria.addOrder(Order.asc(Quote.LABEL_ID));


		criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		List<Quote> quoteList=criteria.list();


		Long totalCount = getTotalCount(ObjectTypes.QUOTE);

		Paginator<Quote> quotes = new Paginator<>(quoteList, totalCount);
		return quotes;
	}

}
