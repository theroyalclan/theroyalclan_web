package com.royalclan.persistence.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Quote;
import com.royalclan.model.Template;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.TemplateDAO;

public class TemplateDAOImpl extends BaseDAOImpl implements TemplateDAO {

	private static TemplateDAO INSTANCE = null;

	private TemplateDAOImpl() {
	}

	public static TemplateDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TemplateDAOImpl();
		}
		return INSTANCE;
	}
	
	@Override
	public Paginator<Template> getTemplates(PaginationInput input) {
		int pageNo = input.getPageNo();
		int pageSize = input.getPageSize();

		int skipCount = (pageNo - 1) * pageSize;  
		Criteria criteria=createCustomCriteria(Template.class);
		criteria.addOrder(Order.asc(Template.LABEL_ID));


		criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		List<Template> consultantList=criteria.list();


		Long totalCount = getTotalCount(ObjectTypes.TEMPLATE);

		Paginator<Template> quotes = new Paginator<>(consultantList, totalCount);
		return quotes;
	}

}
