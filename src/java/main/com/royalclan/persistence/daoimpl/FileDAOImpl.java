package com.royalclan.persistence.daoimpl;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.royalclan.model.File;
import com.royalclan.persistence.dao.FileDAO;

public class FileDAOImpl extends BaseDAOImpl implements FileDAO {

	private static FileDAO INSTANCE = null;

	private FileDAOImpl() {
	}

	public static FileDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new FileDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public List<File> getFiles(Set<Long> fileIds) {
		// TODO Auto-generated method stub
		
		Criteria criteria = createCustomCriteria(File.class);
		criteria.add(Restrictions.in(File.LABEL_ID, fileIds));
		
		
		return criteria.list();
	}

}
