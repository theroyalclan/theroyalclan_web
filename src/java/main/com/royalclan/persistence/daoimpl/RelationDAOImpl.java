package com.royalclan.persistence.daoimpl;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.royalclan.model.Relation;
import com.royalclan.persistence.dao.RelationDAO;

public class RelationDAOImpl extends BaseDAOImpl implements RelationDAO {

	private static RelationDAO INSTANCE = null;

	private RelationDAOImpl() {
	}

	public static RelationDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RelationDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public List<Relation> getAllRelations() {
		// TODO Auto-generated method stub
		
		Criteria criteria = createCustomCriteria(Relation.class);
		
		return criteria.list();
	}
	
	@Override
	public List<Relation> getRelations(Set<Long> relationIds) {
		// TODO Auto-generated method stub
		
		Criteria criteria = createCustomCriteria(Relation.class);
		criteria.add(Restrictions.in(Relation.LABEL_ID, relationIds));
		
		
		return criteria.list();
	}


}
