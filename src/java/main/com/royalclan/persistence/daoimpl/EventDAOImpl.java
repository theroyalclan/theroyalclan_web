package com.royalclan.persistence.daoimpl;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.royalclan.model.Event;
import com.royalclan.model.ObjectTypes;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.EventDAO;
import com.royalclan.service.descriptors.EventPaginationInput;

public class EventDAOImpl extends BaseDAOImpl implements EventDAO {

	private static EventDAO INSTANCE = null;

	private EventDAOImpl() {
	}

	public static EventDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EventDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public Paginator<Event> getEvents(EventPaginationInput eventPaginationInput) {
		// TODO Auto-generated method stub
		int pageNo = eventPaginationInput.getPageNo();
		int pageSize = eventPaginationInput.getPageSize();

		int skipCount = (pageNo - 1) * pageSize;  
		Criteria criteria=createCustomCriteria(Event.class);
		
		if(eventPaginationInput.getUserId()!=0){
			criteria.add(Restrictions.eq(Event.LABEL_OWNER_ID, eventPaginationInput.getUserId()));
		}
		if(eventPaginationInput.isUpcoming()){
			criteria.add(Restrictions.gt(Event.LABEL_DATE_OF_EVENT, new java.sql.Date(Calendar.getInstance().getTimeInMillis())));
			criteria.addOrder(Order.asc(Event.LABEL_DATE_OF_EVENT)).addOrder(Order.asc(Event.LABEL_TIME_OF_EVENT));
		}
		else{
			criteria.add(Restrictions.le(Event.LABEL_DATE_OF_EVENT, new java.sql.Date(Calendar.getInstance().getTimeInMillis())));
			criteria.addOrder(Order.desc(Event.LABEL_DATE_OF_EVENT)).addOrder(Order.desc(Event.LABEL_TIME_OF_EVENT));
		}
		criteria.setProjection(Projections.rowCount());
		Long count = (Long) criteria.uniqueResult();
		criteria.setProjection(null);

		criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		List<Event> eventList=criteria.list();


//		Long totalCount = getTotalCount(ObjectTypes.EVENT);

		Paginator<Event> quotes = new Paginator<>(eventList, count);
		return quotes;
	}

}
