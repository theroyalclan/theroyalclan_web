package com.royalclan.persistence.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Wish;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.WishDAO;

public class WishDAOImpl extends BaseDAOImpl implements WishDAO {

	private static WishDAO INSTANCE = null;

	private WishDAOImpl() {
	}

	public static WishDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new WishDAOImpl();
		}
		return INSTANCE;
	}

	@Override
	public Paginator<Wish> getWishes(PaginationInput input) {
		int pageNo = input.getPageNo();
		int pageSize = input.getPageSize();

		int skipCount = (pageNo - 1) * pageSize;  
		Criteria criteria=createCustomCriteria(Wish.class);
		criteria.addOrder(Order.asc(Wish.LABEL_CREATED_TIME));


		criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		List<Wish> consultantList=criteria.list();


		Long totalCount = getTotalCount(ObjectTypes.WISH);

		Paginator<Wish> quotes = new Paginator<>(consultantList, totalCount);
		return quotes;
	}


}
