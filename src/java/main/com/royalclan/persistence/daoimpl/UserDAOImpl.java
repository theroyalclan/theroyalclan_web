package com.royalclan.persistence.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.royalclan.exception.ExceptionCodes;
import com.royalclan.exception.ExceptionMessages;
import com.royalclan.exception.UserException;
import com.royalclan.model.ObjectTypes;
import com.royalclan.model.Template;
import com.royalclan.model.User;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;
import com.royalclan.persistence.dao.UserDAO;
import com.royalclan.persistence.session.SessionFactoryUtil;

public class UserDAOImpl extends BaseDAOImpl implements UserDAO {

	private static UserDAO INSTANCE = null;

	private UserDAOImpl() {
	}

	public static UserDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UserDAOImpl();
		}
		return INSTANCE;
	}
	@Override
	public User getUserByEmail(String email) throws UserException {
		Session session = null;
		List<User> list = null;
		Transaction tx = null;
		try {
			session = getSession();
			if (null == session) {
				session = SessionFactoryUtil.getInstance().openSession();
				tx = SessionFactoryUtil.getInstance().beginTransaction(session);
			}
			Criteria createCriteria = session.createCriteria(User.class);
			createCriteria.add(Restrictions.eq("email", email));
			createCriteria.add(Restrictions.eq("deleted", false));
			list = createCriteria.list();
			if (list.size() == 0) {
				throw new UserException(ExceptionCodes.USER_DOESNOT_EXIST, ExceptionMessages.USER_DOESNOT_EXIST);
			}

		} finally {

			try {
				if (tx != null) {
					tx.commit();
					if (session.isConnected())
						session.close();
				}
			} catch (HibernateException e) {

				e.printStackTrace();
			}
		}
		return list.iterator().next();

	}
	
	@Override
	public User getLoggedInUser(long userId) {
		 Session session = null;
	     List<User> list = null;
	     Transaction tx = null;
	     try {
	      session = getSession();
	      if (null == session) {
	       session = SessionFactoryUtil.getInstance().openSession();
	       tx = SessionFactoryUtil.getInstance().beginTransaction(session);
	      }
	      Criteria createCriteria = session.createCriteria(User.class);
	      createCriteria.add(Restrictions.eq("id", userId));
	      list = createCriteria.list();
	      if (list.size() == 0) {
//	       throw new UserException(ExceptionCodes.USER_DOESNOT_EXIST, ExceptionMessages.USER_DOESNOT_EXIST);
	        } 
	     }finally {
	        try {
	         if (tx != null) {
	          tx.commit();
	          if (session.isConnected())
	           session.close();
	         }
	        } catch (HibernateException e) {
	         e.printStackTrace();
	        }
	        }
	       return list.iterator().next();
	}

	@Override
	public boolean isDuplicateEmail(User user) {
		// TODO Auto-generated method stub
		Criteria criteria = createCriteria(User.class);
		criteria.add(Restrictions.and(Restrictions.eq(User.LABEL_EMAIL, user.getEmail()),Restrictions.isNotNull(User.LABEL_PASSWORD)));
		if(user.getId()!=0)
			criteria.add(Restrictions.ne(User.LABEL_ID, user.getId()));
		criteria.setProjection(Projections.rowCount());
		long count = (long) criteria.uniqueResult();
		return count>0;
	}
	
	@Override
	public Paginator<User> getUsers(PaginationInput input, long userId) {
		int pageNo = input.getPageNo();
		int pageSize = input.getPageSize();

		int skipCount = (pageNo - 1) * pageSize;  
		Criteria criteria=createCustomCriteria(User.class);
		criteria.addOrder(Order.asc(User.LABEL_NICK_NAME));
		criteria.add(Restrictions.ne(User.LABEL_ID, userId));

		criteria.setFirstResult(skipCount).setMaxResults(pageSize);
		List<User> consultantList=criteria.list();


		Long totalCount = getTotalCount(ObjectTypes.USER);

		Paginator<User> quotes = new Paginator<>(consultantList, totalCount);
		return quotes;
	}
}
