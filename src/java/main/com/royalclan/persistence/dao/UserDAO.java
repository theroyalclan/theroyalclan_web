/**
 * 
 */
package com.royalclan.persistence.dao;

import com.royalclan.exception.UserException;
import com.royalclan.model.User;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;


/**
 * 
 * 
 */
public interface UserDAO extends BaseDAO{

	public User getUserByEmail(String email) throws UserException;

	public User getLoggedInUser(long userId);

	public boolean isDuplicateEmail(User user);

	Paginator<User> getUsers(PaginationInput input, long userId);


	
}
