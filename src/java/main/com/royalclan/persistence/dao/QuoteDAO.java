package com.royalclan.persistence.dao;

import com.royalclan.model.Quote;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;

public interface QuoteDAO extends BaseDAO {

	Paginator<Quote> getQuotes(PaginationInput paginationInput);

}
