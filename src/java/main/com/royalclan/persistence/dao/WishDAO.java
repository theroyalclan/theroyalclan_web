package com.royalclan.persistence.dao;

import com.royalclan.model.Wish;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;

public interface WishDAO extends BaseDAO {

	Paginator<Wish> getWishes(PaginationInput input);

}
