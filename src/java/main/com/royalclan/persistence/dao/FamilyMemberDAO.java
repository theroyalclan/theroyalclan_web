package com.royalclan.persistence.dao;

import com.royalclan.model.FamilyMember;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;

public interface FamilyMemberDAO extends BaseDAO {
	
	public Paginator<FamilyMember> getFamilyMembers(PaginationInput input, long userId);

}
