package com.royalclan.persistence.dao;

import com.royalclan.model.Template;
import com.royalclan.pagination.PaginationInput;
import com.royalclan.pagination.Paginator;

public interface TemplateDAO extends BaseDAO {

	Paginator<Template> getTemplates(PaginationInput input);

}
