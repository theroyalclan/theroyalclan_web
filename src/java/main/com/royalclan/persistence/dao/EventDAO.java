package com.royalclan.persistence.dao;

import com.royalclan.model.Event;
import com.royalclan.pagination.Paginator;
import com.royalclan.service.descriptors.EventPaginationInput;

public interface EventDAO extends BaseDAO {
	
	public Paginator<Event> getEvents(EventPaginationInput eventPaginationInput);
	


}
