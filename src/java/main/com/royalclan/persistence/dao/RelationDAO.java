package com.royalclan.persistence.dao;

import java.util.List;
import java.util.Set;

import com.royalclan.model.Relation;

public interface RelationDAO extends BaseDAO {

	List<Relation> getAllRelations();

	List<Relation> getRelations(Set<Long> relationIds);

}
