package com.royalclan.persistence.dao;

import java.util.List;
import java.util.Set;

import com.royalclan.model.EventType;

public interface EventTypeDAO extends BaseDAO {

	List<EventType> getEventTypes(Set<Long> fileIds);

}
