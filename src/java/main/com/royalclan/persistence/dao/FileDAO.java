package com.royalclan.persistence.dao;

import java.util.List;
import java.util.Set;

import com.royalclan.model.File;

public interface FileDAO extends BaseDAO {

	List<File> getFiles(Set<Long> fileIds);

}
