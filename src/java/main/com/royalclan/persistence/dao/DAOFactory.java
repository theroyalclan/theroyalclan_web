package com.royalclan.persistence.dao;

import com.royalclan.persistence.daoimpl.EventDAOImpl;
import com.royalclan.persistence.daoimpl.EventTypeDAOImpl;
import com.royalclan.persistence.daoimpl.FamilyCodeDAOImpl;
import com.royalclan.persistence.daoimpl.FamilyMemberDAOImpl;
import com.royalclan.persistence.daoimpl.FileDAOImpl;
import com.royalclan.persistence.daoimpl.QuoteDAOImpl;
import com.royalclan.persistence.daoimpl.RelationDAOImpl;
import com.royalclan.persistence.daoimpl.TemplateDAOImpl;
import com.royalclan.persistence.daoimpl.UserDAOImpl;
import com.royalclan.persistence.daoimpl.WishDAOImpl;

/**
 * DAO Factory. Handlers use this factory to get appropriate DAO.
 * 
 * @author
 * 
 */
public class DAOFactory {
	private static DAOFactory INSTANCE = null;

	private DAOFactory() {
	}

	public static DAOFactory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DAOFactory();
		}
		return INSTANCE;
	}

	public UserDAO getUserDAO() {
		return UserDAOImpl.getInstance();
	}

	public EventDAO getEventDAO() {
		return EventDAOImpl.getInstance();
	}

	public EventTypeDAO getEventTypeDAO() {
		return EventTypeDAOImpl.getInstance();
	}

	public FamilyCodeDAO getFamilyCodeDAO() {
		return FamilyCodeDAOImpl.getInstance();
	}

	public FamilyMemberDAO getFamilyMemberDAO() {
		return FamilyMemberDAOImpl.getInstance();
	}

	public FileDAO getFileDAO() {
		return FileDAOImpl.getInstance();
	}

	public QuoteDAO getQuoteDAO() {
		return QuoteDAOImpl.getInstance();
	}

	public RelationDAO getRelationDAO() {
		return RelationDAOImpl.getInstance();
	}

	public TemplateDAO getTemplateDAO() {
		return TemplateDAOImpl.getInstance();
	}
	
	public WishDAO getWishDAO() {
		return WishDAOImpl.getInstance();
	}

}
