package com.royalclan.persistence.dao;

/**
 * @author 
 */


import java.util.List;

import org.hibernate.Session;

import com.royalclan.exception.ObjectNotFoundException;
import com.royalclan.model.BaseObject;

/*
 * These are the methods we want to expose to business handlers for direct use. So they can call e.g. saveObject method
 * on the corresponding DAOs
 */
public interface BaseDAO {

 public BaseObject saveObject(BaseObject persistentObject);

 public BaseObject update(BaseObject persistentObject);

 public List<BaseObject> save(List<BaseObject> persistentObjects);

 public BaseObject getObjectById(long id, int objectType) throws ObjectNotFoundException;
 
 public Session getSession();
 
	
	public long getTotalCount(int objectType);
	
	public boolean deleteObjectById(long id, int objectType);
}